package com.company;


import java.util.ArrayList;
import java.util.regex.Pattern;

public class Roster {

    //define an ArrayList
    static ArrayList<Student> myStudents = new ArrayList<>();

    //main method, entry point of program
    public static void main(String args[]){
        //Add data
        Roster.addStudent("1","John","smith","John1989@gmail.com",
                20, 88,79, 59);
        addStudent("2","suzan","Erickson","Erickson_1990@gmailcom",
                19, 91,72, 85);
        addStudent("3","Jack","Napoli","The_lawyer99yahoo.com",
                19, 85,84, 87);
        addStudent("4","Erin","Black","Erin.black@comcast.net",
                22, 91,98, 82);
        addStudent("5","Nahatolly","Bah Bioh","nbahbio@wgu.edu",
                38, 80,90, 85);

        print_all();
        moveLine();
        print_invalid_emails();
        moveLine();
        for(Student currentStudent: myStudents){
            print_average_grade(currentStudent.getStudentID());
        }
        moveLine();
        remove("3");
        moveLine();
        remove("3");

    }

    //add a student to the roster
    public static void addStudent(String studentID, String first_name, String last_name,
                                  String email_address, int age, int grade1, int grade2, int grade3){
            int grades[] = {grade1, grade2, grade3};

            Student newStudent = new Student(studentID, first_name, last_name, email_address, age, grades);
            myStudents.add(newStudent);
    }

    //print all students
    public static void print_all(){
        //loop through myStudent ArrayList
        for(int i = 0; i < myStudents.size(); i++){
            myStudents.get(i).print();
        }
    }

    //print average grades by student ID
    public static void print_average_grade(String studentID){
        for (Student chosenStudent : myStudents){
            if (chosenStudent.getStudentID().equals(studentID)){
                Double average = (chosenStudent.getGrades()[0] + chosenStudent.getGrades()[1]
                        + chosenStudent.getGrades()[2]) / 3.0;
                System.out.printf("Average grade for student %s is %5.2f %n",chosenStudent.getStudentID(), average);
                return;
            }
        }
    }

    //Remove student by student ID
    public static void remove(String studentID){
       //loop through myStudent ArrayList
        for (Student chosenStudent : myStudents) {
                    if (chosenStudent.getStudentID().equals(studentID)) {
                        System.out.printf("Sudent %s has been succesfully removed from list!", chosenStudent.getStudentID());
                        myStudents.remove(chosenStudent);
                        return;
                    }
        }
        System.out.println("NO SUCH STUDENT FOUND!");
    }

    //print all invalid emails
    public static void print_invalid_emails(){
        //loop through myStudent ArraList
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";    //Mkyong.com. (2012, October). MYKONG. Retrieved from https://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/
        for (Student currentStudent : myStudents){
            boolean email = Pattern.matches(EMAIL_PATTERN,currentStudent.getEmail_address()); //see the Pattern Class in the Java doc.
              if(!email){
                  System.out.println(currentStudent.getEmail_address() +"\t" + "IS INVALID!");
              }
        }
    }

    public static void moveLine(){
        System.out.println("\n");
    }
}

