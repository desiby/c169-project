 /** WGU C169 Project
 *
 * Student Management
 * @Author Nahatolly Bah Bioh
 * @version 1.0
 * @Since 2017-03-21
 */

package com.company;

public class Student {

    private String studentID;
    private String first_name;
    private String last_name;
    private String email_address;
    private int age;
    private int[] grades;

    /**
     * constructor, call setter methods to to set values
     */

    public Student(String studentID, String first_name, String last_name, String email_address, int age, int...grades){
        setStudentID(studentID);
        setFirst_name(first_name);
        setLast_name(last_name);
        setEmail_address(email_address);
        setGrades(grades);
    }

    // Accessors

    /**
     * get a student ID
     * @return  String
     */
    public String getStudentID(){
        return studentID;
    }

    /**
     * Get the student' first name
     * @return String
     */
    public String getFirst_name(){
        return first_name;
    }

    /**
     * Get the student's Last name
     * @return String
     */
    public String getLast_name(){
        return last_name;
    }

    /**
     * Get the email address
     * @return String
     */
    public String getEmail_address(){
        return email_address;
    }

    /**
     * Get the student's age
     * @return int
     */
    public int getAge() {
        return age;
    }

    /**
     * Get all the student's grades
     *
     */
    public int[] getGrades() {
        return grades;
    }

    // -------  Mutators

    /**
     *
     * @param studentID provide student ID
     */
    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    /**
     *
     * @param first_name provide first name
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     *
     * @param last_name provide last name
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     *
     * @param email_address provide email address
     */
    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    /**
     *
     * @param age provide age
     */
    public void setAge(byte age) {
        this.age = age;
    }

    /**
     *
     * @param grades provide grades
     */
    public void setGrades(int[] grades) {
        this.grades = grades;
    }


    //--------- printing Student

    public void print(){
        System.out.println("Student ID: "+ getStudentID() + "\tFirst Name:\t"+ getFirst_name()
                + "\tLast Name:\t"+ getLast_name()+ "\t\tGrades:\t"
                + getGrades()[0] + " "
                + getGrades()[1] + " "
                + getGrades()[2]);
    }


}
